<?php

require_once '../model/ArCondicionado.php';
require_once '../model/Temperatura.php';

session_start();



if($_SESSION['ar']->getStatus()){
    echo "Ligado";
    echo $_SESSION['ar']->getTemperatura();
    if($_SESSION['ar']->getTemperatura()>$_SESSION['temp']->getTemperaturaAtual()){
        $_SESSION['temp']->aumentar(1);
    }else{
        $_SESSION['temp']->diminuir(1);
    }
}else{
    echo "Desligado";
}