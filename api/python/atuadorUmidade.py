# Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
# SPDX-License-Identifier: MIT-0

# modified by Angelo Duarte, Aug/2020

import time
import json
import requests
import AWSIoTPythonSDK.MQTTLib as AWSIoTPyMQTT

# Define ENDPOINT, CLIENT_ID, PATH_TO_CERT, PATH_TO_KEY, PATH_TO_ROOT, MESSAGE, TOPIC, and RANGE
ENDPOINT = "a3jfmm2t0ngsp7-ats.iot.us-east-1.amazonaws.com"
CLIENT_ID = "atuadorUmidade"
PATH_TO_CERT = "python/certificados/atuadorUmidade/ec4872f6d3-certificate.pem.crt"
PATH_TO_KEY = "python/certificados/atuadorUmidade/ec4872f6d3-private.pem.key"
PATH_TO_ROOT = "python/certificados/AmazonRootCA1.pem.txt"
TOPIC_TO_SUBSCRIBE = "atuador/umidade"
RANGE = 50

def customCallback(client, userdata, message):
	print(message.payload)
	##data_json = json.dumps(message.payload)
	##print(data_json)
	#response = requests.post("http://localhost/ar-condicionado-iot/api/receber-umidade.php", ar=data_json)


myAWSIoTMQTTClient = AWSIoTPyMQTT.AWSIoTMQTTClient(CLIENT_ID)
myAWSIoTMQTTClient.configureEndpoint(ENDPOINT, 8883)
myAWSIoTMQTTClient.configureCredentials(PATH_TO_ROOT, PATH_TO_KEY, PATH_TO_CERT)


myAWSIoTMQTTClient.connect()
for i in range (RANGE):
	myAWSIoTMQTTClient.subscribe(TOPIC_TO_SUBSCRIBE, 0, customCallback)
	print("Olá")
myAWSIoTMQTTClient.disconnect()
