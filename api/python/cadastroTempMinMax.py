# Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
# SPDX-License-Identifier: MIT-0


import time
import json
import AWSIoTPythonSDK.MQTTLib as AWSIoTPyMQTT

# Define ENDPOINT, CLIENT_ID, PATH_TO_CERT, PATH_TO_KEY, PATH_TO_ROOT, MESSAGE, TOPIC, and RANGE
ENDPOINT = "a3jfmm2t0ngsp7-ats.iot.us-east-1.amazonaws.com"
CLIENT_ID = "cadastroTempMinMax"
PATH_TO_CERT = "certificados/cadastroTempMinMax/37e9c96815-certificate.pem.crt"
PATH_TO_KEY = "certificados/cadastroTempMinMax/37e9c96815-private.pem.key"
PATH_TO_ROOT = "certificados/AmazonRootCA1.pem.txt"

TOPIC = "sensor/temperatura-cofre"
def definirMinMax(tempMin, tempMax):    
    myAWSIoTMQTTClient = AWSIoTPyMQTT.AWSIoTMQTTClient(CLIENT_ID)
    myAWSIoTMQTTClient.configureEndpoint(ENDPOINT, 8883)
    myAWSIoTMQTTClient.configureCredentials(PATH_TO_ROOT, PATH_TO_KEY, PATH_TO_CERT)

    myAWSIoTMQTTClient.connect()
    myAWSIoTMQTTClient.publish(TOPIC, json.dumps({"tempMin":tempMin, "tempMax": tempMax }), 1) 
    myAWSIoTMQTTClient.disconnect()
