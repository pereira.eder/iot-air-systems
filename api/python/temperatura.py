# Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
# SPDX-License-Identifier: MIT-0

import sys
import time
import json
import AWSIoTPythonSDK.MQTTLib as AWSIoTPyMQTT
import requests



def buscarTemperatura():
    r = requests.get('https://ar-condicionado-iot.herokuapp.com/api/json/temperatura.json')
    # Define ENDPOINT, CLIENT_ID, PATH_TO_CERT, PATH_TO_KEY, PATH_TO_ROOT, MESSAGE, TOPIC, and RANGE
    ENDPOINT = "a3jfmm2t0ngsp7-ats.iot.us-east-1.amazonaws.com"
    CLIENT_ID = "sensorTemperatura"
    PATH_TO_CERT = "certificados/sensorTemperatura/055631526e-certificate.pem.crt"
    PATH_TO_KEY = "certificados/sensorTemperatura/055631526e-private.pem.key"
    PATH_TO_ROOT = "certificados/AmazonRootCA1.pem.txt"
    TOPIC = "sensor/temperatura-cofre"
    myAWSIoTMQTTClient = AWSIoTPyMQTT.AWSIoTMQTTClient(CLIENT_ID)
    myAWSIoTMQTTClient.configureEndpoint(ENDPOINT, 8883)
    myAWSIoTMQTTClient.configureCredentials(PATH_TO_ROOT, PATH_TO_KEY, PATH_TO_CERT)
    myAWSIoTMQTTClient.connect()
    myAWSIoTMQTTClient.publish(TOPIC, r.text, 1)
    myAWSIoTMQTTClient.disconnect()

