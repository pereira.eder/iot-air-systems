# Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
# SPDX-License-Identifier: MIT-0


import time
import json
import AWSIoTPythonSDK.MQTTLib as AWSIoTPyMQTT

# Define ENDPOINT, CLIENT_ID, PATH_TO_CERT, PATH_TO_KEY, PATH_TO_ROOT, MESSAGE, TOPIC, and RANGE
ENDPOINT = "a3jfmm2t0ngsp7-ats.iot.us-east-1.amazonaws.com"
CLIENT_ID = "sensorUmidade"
PATH_TO_CERT = "certificados/sensorUmidade/9ab761ad19-certificate.pem.crt"
PATH_TO_KEY = "certificados/sensorUmidade/9ab761ad19-private.pem.key"
PATH_TO_ROOT = "certificados/AmazonRootCA1.pem.txt"
umidade = 25
TOPIC = "sensor/umidade"


myAWSIoTMQTTClient = AWSIoTPyMQTT.AWSIoTMQTTClient(CLIENT_ID)
myAWSIoTMQTTClient.configureEndpoint(ENDPOINT, 8883)
myAWSIoTMQTTClient.configureCredentials(PATH_TO_ROOT, PATH_TO_KEY, PATH_TO_CERT)

myAWSIoTMQTTClient.connect()
myAWSIoTMQTTClient.publish(TOPIC, json.dumps({"umidade":umidade }), 1) 
myAWSIoTMQTTClient.disconnect()
