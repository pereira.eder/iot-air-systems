<?php
require_once '../model/Temperatura.php'; 
session_start(); 
$arr = array('temperatura'=>$_SESSION['temp']->getTemperaturaAtual());

$dados_json = json_encode($arr);
$arquivo = "json/temperatura.json";
$handle = fopen($arquivo,'w');
fseek($handle, 0);
fwrite($handle,$dados_json);
fclose ($handle);