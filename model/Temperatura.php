<?php

class Temperatura
{
    private $time;
    private $temperaturaAtual;
    
    
    
    public function __construct() {
        $this->setTemperaturaAtual(21);
    }
    
    /**
     * @return mixed
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * @return mixed
     */
    public function getTemperaturaAtual()
    {
        return $this->temperaturaAtual;
    }

    /**
     * @param mixed $time
     */
    public function setTime($time)
    {
        $this->time = $time;
    }

    /**
     * @param mixed $temperaturaAtual
     */
    public function setTemperaturaAtual($temperaturaAtual)
    {
        $this->temperaturaAtual = $temperaturaAtual;
    }
    
    
    public function aumentar($dif){
        $this->setTemperaturaAtual($this->getTemperaturaAtual() + $dif);
    }
    public function diminuir($dif){
        $this->setTemperaturaAtual($this->getTemperaturaAtual() - $dif);
    }
}

